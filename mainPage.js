import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
  Image,
  StatusBar,
  ScrollView,
  Button,
  ImageBackground,
  Dimensions,
} from "react-native";
import { CheckBox } from "native-base";
import Colors from "./Colors";
import { AntDesign } from "@expo/vector-icons";
import { Entypo } from "@expo/vector-icons";
import Modal from "react-native-modal";

class mainPage extends React.Component {
  state = {
    isModal: false,
    touchModal: false,
    selected1: false,
    selected2: false,
    selected3: false,
    selected4: false,
    selected5: false,
    selected6: false,
    selected7: false,
    selected8: false,
    selected9: false,
    selected10: false,
    selected11: false,
    selected12: false,
    selected13: false,
    selected14: false,
    selected15: false,
    selected16: false,
    selected17: false,
  };
  _showModal = () => this.setState({ isModal: true });
  _hideModal = () => this.setState({ isModal: false });
  _showTouchModal = () => this.setState({ touchModal: true });
  _hideTouchModal = () => this.setState({ touchModal: false });
  _clearOption = () => {
    if (this.state.selected1) {
      this.setState({ selected1: false });
    }
    if (this.state.selected2) {
      this.setState({ selected2: false });
    }
    if (this.state.selected3) {
      this.setState({ selected3: false });
    }
    if (this.state.selected4) {
      this.setState({ selected4: false });
    }
    if (this.state.selected5) {
      this.setState({ selected5: false });
    }
    if (this.state.selected6) {
      this.setState({ selected6: false });
    }
    if (this.state.selected7) {
      this.setState({ selected7: false });
    }
    if (this.state.selected8) {
      this.setState({ selected8: false });
    }
    if (this.state.selected9) {
      this.setState({ selected9: false });
    }
    if (this.state.selected10) {
      this.setState({ selected10: false });
    }
    if (this.state.selected11) {
      this.setState({ selected11: false });
    }
    if (this.state.selected12) {
      this.setState({ selected12: false });
    }
    if (this.state.selected13) {
      this.setState({ selected13: false });
    }
    if (this.state.selected14) {
      this.setState({ selected14: false });
    }
    if (this.state.selected15) {
      this.setState({ selected15: false });
    }
    if (this.state.selected16) {
      this.setState({ selected16: false });
    }
    if (this.state.selected17) {
      this.setState({ selected17: false });
    }
  };
  render() {
    const {
      isModal,
      touchModal,
      selected1,
      selected2,
      selected3,
      selected4,
      selected5,
      selected6,
      selected7,
      selected8,
      selected9,
      selected10,
      selected11,
      selected12,
      selected13,
      selected14,
      selected15,
      selected16,
      selected17,
    } = this.state;
    return (
      <View style={{ flex: 1, backgroundColor: "#FFFCF9" }}>
        <SafeAreaView
          style={{
            paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0, //對齊上面
            flexDirection: "row", //排列方向:橫的
          }}
        >
          <View style={styles.titlebox}>
            <Text style={styles.title}>CCU FOODY</Text>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate("myList");
              }}
            >
              <View style={styles.favorite}>
                <AntDesign name="heart" size={24 * rem} color="white" />
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate("settingPage");
              }}
            >
              <View style={styles.setting}>
                <Entypo
                  name="dots-three-vertical"
                  size={24 * rem}
                  color="white"
                />
              </View>
            </TouchableOpacity>
          </View>
        </SafeAreaView>
        <Text style={styles.inline}>隨機挑選</Text>
        <TouchableOpacity onPress={this._showModal}>
          <Text style={styles.select}>篩選</Text>
          <Modal visible={isModal}>
            <View style={styles.selectHere}>
              <View style={styles.selectTitle}>
                <TouchableOpacity onPress={this._hideModal}>
                  <View style={styles.clearstyle}>
                    <AntDesign name="close" size={32 * rem} color="#F5BC5C" />
                  </View>
                </TouchableOpacity>
                <Text style={styles.selectTitlein}>篩選</Text>
                <TouchableOpacity onPress={this._clearOption}>
                  <Text style={styles.selectClear}>全部清除</Text>
                </TouchableOpacity>
              </View>
              <Text style={styles.selectLine}></Text>
              <ScrollView>
                <Text style={styles.selectT}>分類</Text>
                <View style={styles.item}>
                  <Text style={styles.checkBoxTxt}>中式</Text>
                  <View style={styles.checkBox}>
                    <CheckBox
                      checked={selected1}
                      color={this.state.selected1 ? "#F5BC5C" : "#ADADAD"}
                      onPress={() => this.setState({ selected1: !selected1 })}
                    />
                  </View>
                </View>
                <View style={styles.item}>
                  <Text style={styles.checkBoxTxt}>西式</Text>
                  <View style={styles.checkBox}>
                    <CheckBox
                      checked={selected2}
                      color={this.state.selected2 ? "#F5BC5C" : "#ADADAD"}
                      onPress={() => this.setState({ selected2: !selected2 })}
                    />
                  </View>
                </View>
                <View style={styles.item}>
                  <Text style={styles.checkBoxTxt}>日韓</Text>
                  <View style={styles.checkBox}>
                    <CheckBox
                      checked={selected3}
                      color={this.state.selected3 ? "#F5BC5C" : "#ADADAD"}
                      onPress={() => this.setState({ selected3: !selected3 })}
                    />
                  </View>
                </View>
                <View style={styles.item}>
                  <Text style={styles.checkBoxTxt}>飲料</Text>
                  <View style={styles.checkBox}>
                    <CheckBox
                      checked={selected4}
                      color={this.state.selected4 ? "#F5BC5C" : "#ADADAD"}
                      onPress={() => this.setState({ selected4: !selected4 })}
                    />
                  </View>
                </View>
                <View style={styles.item}>
                  <Text style={styles.checkBoxTxt}>甜點</Text>
                  <View style={styles.checkBox}>
                    <CheckBox
                      checked={selected5}
                      color={this.state.selected5 ? "#F5BC5C" : "#ADADAD"}
                      onPress={() => this.setState({ selected5: !selected5 })}
                    />
                  </View>
                </View>
                <View style={styles.item}>
                  <Text style={styles.checkBoxTxt}>宵夜</Text>
                  <View style={styles.checkBox}>
                    <CheckBox
                      checked={selected6}
                      color={this.state.selected6 ? "#F5BC5C" : "#ADADAD"}
                      onPress={() => this.setState({ selected6: !selected6 })}
                    />
                  </View>
                </View>
                <Text style={styles.selectLine}></Text>
                <Text style={styles.selectT}>餐點</Text>
                <View style={styles.item}>
                  <Text style={styles.checkBoxTxt}>早餐</Text>
                  <View style={styles.checkBox}>
                    <CheckBox
                      checked={selected7}
                      color={this.state.selected7 ? "#F5BC5C" : "#ADADAD"}
                      onPress={() => this.setState({ selected7: !selected7 })}
                    />
                  </View>
                </View>
                <View style={styles.item}>
                  <Text style={styles.checkBoxTxt}>便當</Text>
                  <View style={styles.checkBox}>
                    <CheckBox
                      checked={selected8}
                      color={this.state.selected8 ? "#F5BC5C" : "#ADADAD"}
                      onPress={() => this.setState({ selected8: !selected8 })}
                    />
                  </View>
                </View>
                <View style={styles.item}>
                  <Text style={styles.checkBoxTxt}>簡餐</Text>
                  <View style={styles.checkBox}>
                    <CheckBox
                      checked={selected9}
                      color={this.state.selected9 ? "#F5BC5C" : "#ADADAD"}
                      onPress={() => this.setState({ selected9: !selected9 })}
                    />
                  </View>
                </View>
                <View style={styles.item}>
                  <Text style={styles.checkBoxTxt}>麵食</Text>
                  <View style={styles.checkBox}>
                    <CheckBox
                      checked={selected10}
                      color={this.state.selected10 ? "#F5BC5C" : "#ADADAD"}
                      onPress={() => this.setState({ selected10: !selected10 })}
                    />
                  </View>
                </View>
                <View style={styles.item}>
                  <Text style={styles.checkBoxTxt}>滷味</Text>
                  <View style={styles.checkBox}>
                    <CheckBox
                      checked={selected11}
                      color={this.state.selected11 ? "#F5BC5C" : "#ADADAD"}
                      onPress={() => this.setState({ selected11: !selected11 })}
                    />
                  </View>
                </View>
                <View style={styles.item}>
                  <Text style={styles.checkBoxTxt}>炸物</Text>
                  <View style={styles.checkBox}>
                    <CheckBox
                      checked={selected12}
                      color={this.state.selected12 ? "#F5BC5C" : "#ADADAD"}
                      onPress={() => this.setState({ selected12: !selected12 })}
                    />
                  </View>
                </View>
                <View style={styles.item}>
                  <Text style={styles.checkBoxTxt}>火鍋</Text>
                  <View style={styles.checkBox}>
                    <CheckBox
                      checked={selected13}
                      color={this.state.selected13 ? "#F5BC5C" : "#ADADAD"}
                      onPress={() => this.setState({ selected13: !selected13 })}
                    />
                  </View>
                </View>
                <Text style={styles.selectLine}></Text>
                <Text style={styles.selectT}>距離</Text>
                <View style={styles.item}>
                  <Text style={styles.checkBoxTxt}>大吃</Text>
                  <View style={styles.checkBox}>
                    <CheckBox
                      checked={selected14}
                      color={this.state.selected14 ? "#F5BC5C" : "#ADADAD"}
                      onPress={() => this.setState({ selected14: !selected14 })}
                    />
                  </View>
                </View>
                <View style={styles.item}>
                  <Text style={styles.checkBoxTxt}>中吃</Text>
                  <View style={styles.checkBox}>
                    <CheckBox
                      checked={selected15}
                      color={this.state.selected15 ? "#F5BC5C" : "#ADADAD"}
                      onPress={() => this.setState({ selected15: !selected15 })}
                    />
                  </View>
                </View>
                <View style={styles.item}>
                  <Text style={styles.checkBoxTxt}>小吃</Text>
                  <View style={styles.checkBox}>
                    <CheckBox
                      checked={selected16}
                      color={this.state.selected16 ? "#F5BC5C" : "#ADADAD"}
                      onPress={() => this.setState({ selected16: !selected16 })}
                    />
                  </View>
                </View>
                <View style={styles.item}>
                  <Text style={styles.checkBoxTxt}>校內</Text>
                  <View style={styles.checkBox}>
                    <CheckBox
                      checked={selected17}
                      color={this.state.selected17 ? "#F5BC5C" : "#ADADAD"}
                      onPress={() => this.setState({ selected17: !selected17 })}
                    />
                  </View>
                </View>
              </ScrollView>
              <View style={{ marginTop: "4%" }}>
                <Button
                  title="確定"
                  color="#F5BC5C"
                  onPress={this._hideModal}
                />
              </View>
            </View>
          </Modal>
        </TouchableOpacity>
        <Modal visible={touchModal}>
          <View style={styles.card}>
            <Text style={styles.buttomtitle}>等等吃...</Text>
            <Text style={styles.answer}>八鍋</Text>
            <Text style={styles.line}></Text>
            <Text style={styles.ok} onPress={this._hideTouchModal}>
              確定
            </Text>
          </View>
        </Modal>
        <Image
          style={styles.background}
          source={require("./assets/background.png")}
        />
        <TouchableOpacity onPress={this._showTouchModal}>
          <ImageBackground
            style={styles.food}
            source={require("./assets/food.png")}
          ></ImageBackground>
          <Image
            style={styles.touchme}
            source={require("./assets/touchme.png")}
          />
        </TouchableOpacity>
      </View>
    );
  }
}

export default mainPage;

const windowWidth = Dimensions.get("window").width;
const rem = windowWidth / 380;

const styles = StyleSheet.create({
  //for check box
  selectT: {
    marginTop: "4%",
    marginBottom: "2%",
    fontSize: 22,
    color: Colors.middleblack,
  },
  item: {
    flexDirection: "row",
    marginTop: "2%",
    backgroundColor: "white",
  },
  checkBox: {
    marginTop: "2.5%",
    marginLeft: "70%",
    justifyContent: "center",
  },
  checkBoxTxt: {
    color: Colors.middleblack,
    fontSize: 20,
  },
  //for selectHere title
  selectHere: {
    backgroundColor: "white",
    padding: 12 * rem,
    width: "90%",
    aspectRatio: 360 / 640,
    position: "absolute",
    marginTop: "20%",
    marginLeft: "5%",
    borderColor: "#F5BC5C",
    borderRadius: 8,
    borderWidth: 2,
  },
  selectTitle: {
    flexDirection: "row",
    width: "100%",
    aspectRatio: 320 / 40,
  },
  clearstyle: {},
  selectTitlein: {
    fontSize: 23 * rem,
    color: Colors.primary,
    marginLeft: "28%",
  },
  selectClear: {
    fontSize: 15 * rem,
    marginTop: "5%",
    color: Colors.primary,
    marginLeft: "32%",
  },
  selectLine: {
    marginTop: "2%",
    height: 1.5 * rem,
    backgroundColor: "#00000040",
  },
  //for screen title area
  titlebox: {
    flexDirection: "row",
    backgroundColor: Colors.primary,
    height: 46 * rem,
    width: "100%",
  },
  title: {
    color: Colors.white,
    marginLeft: "6%",
    marginRight: "34%",
    fontSize: 24 * rem,
    fontWeight: "bold",
    textAlignVertical: "center",
  },
  favorite: {
    marginTop: "25%",
    marginRight: "6%",
  },
  setting: {
    marginTop: "42%",
  },
  //for the random select text
  inline: {
    color: Colors.primary,
    marginLeft: "35.5%",
    fontSize: 28 * rem,
    marginTop: "13.75%",
  },
  //for the select button
  select: {
    color: "#F5BC5C",
    marginLeft: "45%",
    marginTop: "4%",
    backgroundColor: "white",
    width: "13%",
    fontSize: 14 * rem,
    textAlign: "center",
    textAlignVertical: "center",
    aspectRatio: 48 / 28,
    borderColor: "#F5BC5C",
    borderWidth: 1 * rem,
    borderRadius: 3 * rem,
  },
  //for image
  touchme: {
    width: windowWidth * 0.56,
    marginTop: "10%",
  },
  food: {
    width: windowWidth * 0.86,
    aspectRatio: 320 / 340,
    marginLeft: "8%",
    resizeMode: "contain",
    marginTop: "24%",
    position: "absolute",
  },
  background: {
    width: windowWidth,
    aspectRatio: 360 / 210,
    bottom: 0,
    position: "absolute",
  },
  //for Card
  buttomtitle: {
    fontSize: 18 * rem,
    color: Colors.accent,
  },
  answer: {
    fontSize: 30 * rem,
    textAlign: "center",
    color: Colors.primary,
    marginTop: "8%",
    fontWeight: "bold",
  },
  line: {
    height: 2,
    marginTop: "8%",
    backgroundColor: Colors.lightblack,
  },
  ok: {
    fontSize: 18 * rem,
    textAlign: "center",

    color: Colors.primary,
    width: "100%",
    marginTop: "6%",
  },
  card: {
    backgroundColor: "white",
    padding: 12 * rem,
    width: windowWidth * 0.6,
    aspectRatio: 200 / 150,
    position: "absolute",
    marginLeft: "18%",
    borderColor: "#F5BC5C",
    borderRadius: 10,
    borderWidth: 2,
  },
});
