export default {
  primary: "#F5BC5C",
  middleblack: "#00000080",
  lightblack: "#0000001A",
  accent: "#808080",
  white: "#FFFFFF",
};
