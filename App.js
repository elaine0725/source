import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import mainPage from "./mainPage";
import settingPage from "./setting/settingPage.js";
import reportPage from "./setting/reportPage.js";
import manageDefault from "./setting/manageDefault.js";
import editDefault from "./setting/editDefault.js";
import addDefault from "./setting/addDefault.js";
import myList from "./favorite/myList.js";
import editList from "./favorite/editList";
import chooseToEat from "./favorite/chooseToEat.js";
import createName from "./favorite/createName.js";
import storeListRandom from "./favorite/storeListRandom";
import add from "./favorite/add";

const Stack = createStackNavigator();

export default class App extends React.Component {
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator mode="modal" headerMode="none">
          <Stack.Screen name="mainPage" component={mainPage} />
          <Stack.Screen name="settingPage" component={settingPage} />
          <Stack.Screen name="reportPage" component={reportPage} />
          <Stack.Screen name="manageDefault" component={manageDefault} />
          <Stack.Screen name="editDefault" component={editDefault} />
          <Stack.Screen name="addDefault" component={addDefault} />
          <Stack.Screen name="myList" component={myList} />
          <Stack.Screen name="editList" component={editList} />
          <Stack.Screen name="chooseToEat" component={chooseToEat} />
          <Stack.Screen name="createName" component={createName} />
          <Stack.Screen name="storeListRandom" component={storeListRandom} />
          <Stack.Screen name="add" component={add} />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}
