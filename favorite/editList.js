import * as React from "react";
import {
  StyleSheet,
  Text,
  Image,
  View,
  TouchableOpacity,
  Platform,
  StatusBar,
  SafeAreaView,
  TextInput,
  Button,
  Alert,
  Dimensions,
} from "react-native";
import { MaterialIcons } from "@expo/vector-icons";
import { MaterialCommunityIcons } from "@expo/vector-icons";

class editList extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, backgroundColor: "#FFFCF9" }}>
        <SafeAreaView
          style={{
            paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0, //對齊上面
            flexDirection: "row", //排列方向:橫的
          }}
        >
          <View style={styles.titlebox}>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate("myList");
              }}
            >
              <View style={styles.backicon}>
                <MaterialIcons
                  name="arrow-back"
                  size={34 * rem}
                  color="white"
                />
              </View>
            </TouchableOpacity>

            <Text style={styles.upperlinewordbig}>編輯收藏清單</Text>
          </View>
        </SafeAreaView>

        <Text style={styles.inword}>名稱</Text>
        <TextInput
          style={styles.textinput}
          placeholder="清單名稱"
          //onChangeText={setPostText}
          //value={postText}
        />

        <Text style={styles.inword}>餐廳清單選項</Text>

        <View style={styles.choiceBox}>
          <Text style={styles.choiceInput}>餐廳名稱</Text>
          <TouchableOpacity
            onPress={() =>
              Alert.alert("要刪除嗎", "", [{ text: "取消" }, { text: "刪除" }])
            }
          >
            <View style={styles.xxicon}>
              <MaterialCommunityIcons
                name="close-circle"
                size={20 * rem}
                color="#FF8040"
              />
            </View>
          </TouchableOpacity>
        </View>
        <View style={styles.choiceBox}>
          <Text style={styles.choiceInput}>餐廳名稱</Text>
          <TouchableOpacity
            onPress={() =>
              Alert.alert("要刪除嗎", "", [{ text: "取消" }, { text: "刪除" }])
            }
          >
            <View style={styles.xxicon}>
              <MaterialCommunityIcons
                name="close-circle"
                size={20 * rem}
                color="#FF8040"
              />
            </View>
          </TouchableOpacity>
        </View>

        <TouchableOpacity
          style={{ alignItems: "center" }}
          onPress={() => {
            this.props.navigation.navigate("add");
          }}
        >
          <Text style={styles.insert}>新增餐廳</Text>
        </TouchableOpacity>

        <View style={styles.button}>
          <Button
            title="確定"
            color="#F5BC5C"
            onPress={() => {
              // Pass params back to home screen
              this.props.navigation.navigate("myList");
            }}
          />
        </View>
      </View>
    );
  }
}

export default editList;
const windowWidth = Dimensions.get("window").width;
const rem = windowWidth / 380;
const styles = StyleSheet.create({
  //for screen title area
  titlebox: {
    flexDirection: "row",
    backgroundColor: "#F5BC5C",
    height: 46 * rem,
    width: "100%",
    padding: 5 * rem,
  },
  backicon: {
    marginLeft: "10%",
  },
  upperlinewordbig: {
    fontSize: 22 * rem,
    color: "white",
  },

  //editlist
  inword: {
    marginLeft: "6%",
    marginTop: "4%",
    fontSize: 22 * rem,
    color: "#00000080",
  },
  textinput: {
    width: "88%", //寬度
    aspectRatio: 100 / 12, //高度
    backgroundColor: "white",
    borderColor: "#0000001A",
    borderWidth: 0.5,
    fontSize: 18 * rem,
    marginLeft: "6%",
    marginTop: "4%",
  },
  choiceBox: {
    flexDirection: "row",
    marginTop: "2%",
  },
  choiceInput: {
    marginLeft: "6%",
    width: "80%",
    fontSize: 18 * rem,
    color: "#00000080",
  },
  xxicon: {
    marginTop: "45%",
  },
  insert: {
    width: "40%",
    aspectRatio: 156 / 45,
    backgroundColor: "white",
    borderColor: "#F2BA5B",
    borderWidth: 2,
    borderRadius: 5,
    fontSize: 15 * rem,
    color: "#F2BA5B",
    textAlignVertical: "center",
    textAlign: "center",
    marginTop: "4%",
  },
  button: {
    marginLeft: "6%",
    margin: "6%",
    width: "88%",
    position: "absolute",
    bottom: 0,
  },
});
