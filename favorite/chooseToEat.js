import * as React from "react";
import {
  StyleSheet,
  Text,
  Image,
  View,
  TouchableOpacity,
  Platform,
  StatusBar,
  SafeAreaView,
  TextInput,
  Button,
  Dimensions,
} from "react-native";
import { CheckBox } from "native-base";
import { ScrollView } from "react-native-gesture-handler";
import { MaterialIcons } from "@expo/vector-icons";
import { AntDesign } from "@expo/vector-icons";

class chooseToEat extends React.Component {
  state = {
    selected1: false,
  };
  render() {
    const { selected1 } = this.state;
    return (
      <View style={{ flex: 1, backgroundColor: "#FFFCF9" }}>
        <SafeAreaView
          style={{
            paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0, //對齊上面
            flexDirection: "row", //排列方向:橫的
          }}
        >
          <View style={styles.titlebox}>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate("myList")}
            >
              <View style={styles.backicon}>
                <MaterialIcons name="close" size={34 * rem} color="white" />
              </View>
            </TouchableOpacity>

            <Text style={styles.upperlinewordbig}>選擇餐廳</Text>
          </View>
        </SafeAreaView>
        <View style={styles.searchbox}>
          <View style={styles.searchIcon}>
            <AntDesign name="search1" size={28 * rem} color="#00000080" />
          </View>
          <TextInput style={styles.searchInput} placeholder="搜尋餐廳" />
        </View>
        <Text style={styles.Line} />
        <ScrollView>
          <Text style={styles.selectT}>校內</Text>
          <View style={styles.item}>
            <Text style={styles.checkBoxTxt}>餐廳名稱</Text>
            <View style={styles.checkBox}>
              <CheckBox
                checked={selected1}
                color={this.state.selected1 ? "#F5BC5C" : "#ADADAD"}
                onPress={() => this.setState({ selected1: !selected1 })}
              />
            </View>
          </View>
          <View style={styles.item}>
            <Text style={styles.checkBoxTxt}>餐廳名稱</Text>
            <CheckBox style={styles.checkBox} />
          </View>
          <View style={styles.item}>
            <Text style={styles.checkBoxTxt}>餐廳名稱</Text>
            <CheckBox style={styles.checkBox} />
          </View>
          <Text style={styles.selectT}>大吃</Text>
          <View style={styles.item}>
            <Text style={styles.checkBoxTxt}>餐廳名稱</Text>
            <CheckBox style={styles.checkBox} />
          </View>
          <View style={styles.item}>
            <Text style={styles.checkBoxTxt}>餐廳名稱</Text>
            <CheckBox style={styles.checkBox} />
          </View>
          <View style={styles.item}>
            <Text style={styles.checkBoxTxt}>餐廳名稱</Text>
            <CheckBox style={styles.checkBox} />
          </View>
          <Text style={styles.selectT}>中吃</Text>
          <View style={styles.item}>
            <Text style={styles.checkBoxTxt}>餐廳名稱</Text>
            <CheckBox style={styles.checkBox} />
          </View>
          <View style={styles.item}>
            <Text style={styles.checkBoxTxt}>餐廳名稱</Text>
            <CheckBox style={styles.checkBox} />
          </View>
          <View style={styles.item}>
            <Text style={styles.checkBoxTxt}>餐廳名稱</Text>
            <CheckBox style={styles.checkBox} />
          </View>
          <Text style={styles.selectT}>小吃</Text>
          <View style={styles.item}>
            <Text style={styles.checkBoxTxt}>餐廳名稱</Text>
            <CheckBox style={styles.checkBox} />
          </View>
          <View style={styles.item}>
            <Text style={styles.checkBoxTxt}>餐廳名稱</Text>
            <CheckBox style={styles.checkBox} />
          </View>
          <View style={styles.item}>
            <Text style={styles.checkBoxTxt}>餐廳名稱</Text>
            <CheckBox style={styles.checkBox} />
          </View>
          <Text style={styles.selectT}>校內</Text>
          <View style={styles.item}>
            <Text style={styles.checkBoxTxt}>餐廳名稱</Text>
            <CheckBox style={styles.checkBox} />
          </View>
          <View style={styles.item}>
            <Text style={styles.checkBoxTxt}>餐廳名稱</Text>
            <CheckBox style={styles.checkBox} />
          </View>
          <View style={styles.item}>
            <Text style={styles.checkBoxTxt}>餐廳名稱</Text>
            <CheckBox style={styles.checkBox} />
          </View>
        </ScrollView>
        <View style={{ margin: "6%", width: "88%" }}>
          <Button
            title="下一步"
            color="#F5BC5C"
            onPress={() => {
              this.props.navigation.navigate("createName");
            }}
          />
        </View>
      </View>
    );
  }
}

export default chooseToEat;
const windowWidth = Dimensions.get("window").width;
const rem = windowWidth / 380;

const styles = StyleSheet.create({
  //for screen title area
  titlebox: {
    flexDirection: "row",
    backgroundColor: "#F5BC5C",
    height: 46 * rem,
    width: "100%",
    padding: 5 * rem,
  },
  backicon: {
    marginLeft: "10%",
  },
  upperlinewordbig: {
    fontSize: 22 * rem,
    color: "white",
  },
  //for chooseToEat
  searchbox: {
    flexDirection: "row",
    width: "100%",
    marginTop: "4%",
    textAlignVertical: "center",
  },
  searchInput: {
    fontSize: 18 * rem,
    marginLeft: "6%",
  },
  searchIcon: {
    marginLeft: "6%",
  },
  Line: {
    marginTop: "4%",
    height: 2,
    backgroundColor: "#00000040",
  },
  //for checkbox
  selectT: {
    marginTop: "4%",
    marginLeft: "6%",
    fontSize: 20 * rem,
    color: "#00000080",
    marginBottom: "2%",
  },
  item: {
    flexDirection: "row",
    marginTop: "2%",
    marginLeft: "6%",
  },
  checkBox: {
    color: "#0000003F",
    marginTop: "2.5%",
    borderColor: "#00000040",
    borderRadius: 4,
    marginLeft: "60%",
    justifyContent: "center",
  },
  checkBoxTxt: {
    color: "#00000080",
    fontSize: 18 * rem,
  },
});
