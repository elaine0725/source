import * as React from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Platform,
  StatusBar,
  SafeAreaView,
  TextInput,
  Dimensions,
  Button,
} from "react-native";
import { MaterialIcons } from "@expo/vector-icons";

class createName extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, backgroundColor: "#FFFCF9" }}>
        <SafeAreaView
          style={{
            paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0, //對齊上面
            flexDirection: "row", //排列方向:橫的
          }}
        >
          <View style={styles.titlebox}>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate("chooseToEat");
              }}
            >
              <View style={styles.backicon}>
                <MaterialIcons
                  name="arrow-back"
                  size={34 * rem}
                  color="white"
                />
              </View>
            </TouchableOpacity>

            <Text style={styles.upperlinewordbig}>建立收藏清單</Text>
          </View>
        </SafeAreaView>
        <Text style={styles.inword}>名稱</Text>
        <TextInput
          style={styles.textinput}
          placeholder="清單名稱"
          //onChangeText={setPostText}
          //value={postText}
        />
        <View style={styles.confirmbutton}>
          <Button
            title="建立清單"
            color="#F5BC5C"
            onPress={() => {
              this.props.navigation.navigate("myList");
            }}
          />
        </View>
      </View>
    );
  }
}

export default createName;
const windowWidth = Dimensions.get("window").width;
const rem = windowWidth / 380;
const styles = StyleSheet.create({
  //for screen title area
  titlebox: {
    flexDirection: "row",
    backgroundColor: "#F5BC5C",
    height: 46 * rem,
    width: "100%",
    padding: 5 * rem,
  },
  backicon: {
    marginLeft: "10%",
  },
  upperlinewordbig: {
    fontSize: 22 * rem,
    color: "white",
  },
  confirmbutton: {
    margin: "6%",
    width: "88%",
    aspectRatio: 100 / 12,
    marginTop: "4%",
    position: "absolute",
    bottom: 0,
  },
  inword: {
    marginLeft: "6%",
    marginTop: "4%",
    fontSize: 22 * rem,
    color: "#00000080",
  },
  textinput: {
    width: "88%", //寬度
    aspectRatio: 100 / 12, //高度
    backgroundColor: "white",
    borderColor: "#0000001A",
    borderWidth: 0.5,
    fontSize: 18 * rem,
    marginLeft: "6%",
    marginTop: "4%",
  },
});
