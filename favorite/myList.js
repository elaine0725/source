import * as React from "react";
import {
  Text,
  Image,
  View,
  TouchableOpacity,
  Platform,
  StatusBar,
  SafeAreaView,
  Alert,
  StyleSheet,
  Dimensions,
} from "react-native";
import { MaterialIcons } from "@expo/vector-icons";
import { Entypo } from "@expo/vector-icons";
import { AntDesign } from "@expo/vector-icons";

class myList extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, backgroundColor: "#FFFCF9" }}>
        <SafeAreaView //flexgrow:調整每個物件的占比=>1:8:1
          style={{
            paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0, //對齊上面
            flexDirection: "row", //排列方向:橫的
          }}
        >
          <View style={styles.titlebox}>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate("mainPage")}
            >
              <View style={styles.backicon}>
                <MaterialIcons
                  name="arrow-back"
                  size={34 * rem}
                  color="white"
                />
              </View>
            </TouchableOpacity>

            <Text style={styles.upperlinewordbig}>我的收藏清單</Text>
          </View>
        </SafeAreaView>
        <View style={styles.editrow}>
          <TouchableOpacity
            style={styles.input}
            onPress={() => this.props.navigation.navigate("storeListRandom")}
          >
            <Text style={styles.inputword}></Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() =>
              Alert.alert("", "", [
                {
                  text: "編輯",
                  onPress: () => this.props.navigation.navigate("editList"),
                },
                {
                  text: "刪除",
                  onPress: () =>
                    Alert.alert("要刪除嗎?", "", [
                      { text: "取消" },
                      { text: "刪除" },
                    ]),
                },
              ])
            }
          >
            <View style={styles.editicon}>
              <Entypo
                name="dots-three-vertical"
                size={22 * rem}
                color="#00000080"
              />
            </View>
          </TouchableOpacity>
        </View>
        <View
          style={{
            width: "100%",
            alignItems: "flex-end",
            bottom: 0,
            position: "absolute",
          }}
        >
          <TouchableOpacity
            style={styles.plusview}
            onPress={() => this.props.navigation.navigate("chooseToEat")}
          >
            <View style={styles.plusicon}>
              <AntDesign name="plus" size={30 * rem} color="white" />
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default myList;
const windowWidth = Dimensions.get("window").width;
const rem = windowWidth / 380;
const styles = StyleSheet.create({
  //for screen title area
  titlebox: {
    flexDirection: "row",
    backgroundColor: "#F5BC5C",
    height: 46 * rem,
    width: "100%",
    padding: 5 * rem,
  },
  backicon: {
    marginLeft: "10%",
  },
  upperlinewordbig: {
    fontSize: 22 * rem,
    color: "white",
  },
  //mylist
  editrow: {
    flexDirection: "row",
    marginTop: "6%",
  },
  input: {
    marginLeft: "6%",
    aspectRatio: 100 / 13, //高度
    width: "80%", //寬度
    backgroundColor: "#F6EFE4",
    borderRadius: 5,
    justifyContent: "center",
  },
  inputword: {
    fontSize: 20,
    color: "#00000080",
    marginLeft: "4%",
  },
  editicon: {
    marginTop: "12%",
    marginLeft: "12%",
  },
  plusview: {
    backgroundColor: "#F5BC5C",
    width: 60 * rem,
    height: 60 * rem,
    borderRadius: 30 * rem,
    justifyContent: "center",
    alignItems: "center",
    marginBottom: "6%",
    marginRight: "6%",
  },
  plusicon: {},
});
