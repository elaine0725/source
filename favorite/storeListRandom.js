import React, { useState } from "react";
import {
  View,
  StyleSheet,
  Text,
  SafeAreaView,
  StatusBar,
  TouchableOpacity,
  Image,
  ImageBackground,
  Dimensions,
} from "react-native";
import Modal from "react-native-modal";
import Colors from "../Colors";
import { MaterialIcons } from "@expo/vector-icons";

class storeListRandom extends React.Component {
  state = {
    isModal: false,
  };
  _showModal = () => this.setState({ isModal: true });
  _hideModal = () => this.setState({ isModal: false });
  render() {
    const { isModal } = this.state;
    return (
      <View style={{ flex: 1, backgroundColor: "#FFFCF9" }}>
        <SafeAreaView //flexgrow:調整每個物件的占比=>1:8:1
          style={{
            paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0, //對齊上面
            flexDirection: "row", //排列方向:橫的
          }}
        >
          <View style={styles.titlebox}>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate("myList");
              }}
            >
              <View style={styles.backicon}>
                <MaterialIcons
                  name="arrow-back"
                  size={34 * rem}
                  color="white"
                />
              </View>
            </TouchableOpacity>

            <Text style={styles.upperlinewordbig}>收藏清單隨選</Text>
          </View>
        </SafeAreaView>
        <Text style={styles.inline}>中正火鍋</Text>
        <Image
          style={styles.decoration}
          source={require("../assets/decoration.png")}
        />
        <Image
          style={styles.background}
          source={require("../assets/background.png")}
        />
        <TouchableOpacity onPress={this._showModal}>
          <ImageBackground
            style={styles.food}
            source={require("../assets/food1.png")}
          ></ImageBackground>
          <Image
            style={styles.touchme}
            source={require("../assets/touchme.png")}
          />
          <Modal visible={isModal}>
            <View style={styles.card}>
              <Text style={styles.buttomtitle}>等等吃...</Text>
              <Text style={styles.answer}>八鍋</Text>
              <Text style={styles.line}></Text>
              <Text style={styles.ok} onPress={this._hideModal}>
                確定
              </Text>
            </View>
          </Modal>
        </TouchableOpacity>
      </View>
    );
  }
}

export default storeListRandom;
const windowWidth = Dimensions.get("window").width;
const rem = windowWidth / 380;

const styles = StyleSheet.create({
  //for screen title area
  titlebox: {
    flexDirection: "row",
    backgroundColor: "#F5BC5C",
    height: 46 * rem,
    width: "100%",
    padding: 5 * rem,
  },
  backicon: {
    marginLeft: "10%",
  },
  upperlinewordbig: {
    fontSize: 22 * rem,
    color: "white",
  },
  //content
  inline: {
    color: "#F5BC5C",
    marginLeft: "35.5%",
    fontSize: 28 * rem,
    marginTop: "13.75%",
  },
  //for image
  touchme: {
    width: windowWidth * 0.56,
    marginTop: "20%",
  },
  food: {
    width: windowWidth * 0.86,
    aspectRatio: 320 / 340,
    marginLeft: "8%",
    resizeMode: "contain",
    marginTop: "36%",
    position: "absolute",
  },
  background: {
    width: windowWidth,
    aspectRatio: 360 / 210,
    bottom: 0,
    position: "absolute",
  },
  decoration: {
    width: windowWidth * 0.6,
    marginTop: "70%",
    marginLeft: "40%",
    position: "absolute",
    resizeMode: "contain",
  },
  //for card
  buttomtitle: {
    fontSize: 18 * rem,
    color: Colors.accent,
  },
  answer: {
    fontSize: 30 * rem,
    textAlign: "center",
    color: Colors.primary,
    marginTop: "8%",
    fontWeight: "bold",
  },
  line: {
    height: 2,
    marginTop: "8%",
    backgroundColor: Colors.lightblack,
  },
  ok: {
    fontSize: 18 * rem,
    textAlign: "center",

    color: Colors.primary,
    width: "100%",
    marginTop: "6%",
  },
  card: {
    backgroundColor: "white",
    padding: 12 * rem,
    width: windowWidth * 0.6,
    aspectRatio: 200 / 150,
    position: "absolute",
    marginLeft: "18%",
    borderColor: "#F5BC5C",
    borderRadius: 10,
    borderWidth: 2,
  },
});
