import * as React from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Platform,
  StatusBar,
  SafeAreaView,
  TextInput,
  Button,
  Dimensions,
} from "react-native";
import { MaterialIcons } from "@expo/vector-icons";

const reportPage = ({ navigation }) => {
  return (
    <View style={{ flex: 1, backgroundColor: "#FFFCF9" }}>
      <SafeAreaView
        style={{
          paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0, //對齊上面
          flexDirection: "row", //排列方向:橫的
        }}
      >
        <View style={styles.titlebox}>
          <TouchableOpacity onPress={() => navigation.navigate("settingPage")}>
            <View style={styles.backicon}>
              <MaterialIcons name="close" size={34 * rem} color="white" />
            </View>
          </TouchableOpacity>

          <Text style={styles.upperlinewordbig}>問題回報</Text>
        </View>
      </SafeAreaView>
      <Text style={styles.inword}>問題標題</Text>
      <TextInput style={styles.textinput} />
      <View style={{ flexDirection: "row" }}>
        <Text style={styles.inword}>描述內容</Text>
        <Text style={styles.word}>(0/400)</Text>
      </View>
      <TextInput style={styles.textMultiInput} multiline />
      <View style={styles.confirmbutton}>
        <Button
          title="送出"
          color="#F5BC5C"
          onPress={() => {
            navigation.navigate("settingPage");
          }}
        />
      </View>
    </View>
  );
};

export default reportPage;
const windowWidth = Dimensions.get("window").width;
const rem = windowWidth / 380;

const styles = StyleSheet.create({
  //for screen title area
  titlebox: {
    flexDirection: "row",
    backgroundColor: "#F5BC5C",
    height: 46 * rem,
    width: "100%",
    padding: 5 * rem,
  },
  backicon: {
    marginLeft: "10%",
  },
  upperlinewordbig: {
    fontSize: 22 * rem,
    color: "white",
  },
  inword: {
    marginLeft: "6%",
    marginTop: "4%",
    fontSize: 20 * rem,
    color: "#00000080",
  },
  word: {
    marginLeft: "50%",
    marginTop: "6%",
    fontSize: 14 * rem,
    color: "#00000040",
  },
  textinput: {
    width: "88%",
    aspectRatio: 100 / 12,
    backgroundColor: "white",
    borderColor: "#0000001A",
    borderWidth: 0.5,
    borderRadius: 5,
    fontSize: 20 * rem,
    marginLeft: "6%",
    marginTop: "4%",
  },
  textMultiInput: {
    width: "88%",
    aspectRatio: 328 / 160,
    backgroundColor: "white",
    borderColor: "#0000001A",
    borderWidth: 0.5,
    borderRadius: 5,
    fontSize: 20 * rem,
    marginLeft: "6%",
    marginTop: "4%",
  },
  confirmbutton: {
    margin: "6%",
    width: "88%",
    marginTop: "4%",
  },
});
