import * as React from "react";
import {
  StyleSheet,
  Text,
  Dimensions,
  View,
  TouchableOpacity,
  Platform,
  StatusBar,
  SafeAreaView,
  TextInput,
  Button,
  Picker,
} from "react-native";

import { AntDesign } from "@expo/vector-icons";

class editDefault extends React.Component {
  render() {
    state = {
      hand: "big",
    };
    return (
      <View style={{ flex: 1, backgroundColor: "#FFFCF9" }}>
        <SafeAreaView
          style={{
            paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0, //對齊上面
            flexDirection: "row", //排列方向:橫的
          }}
        >
          <View style={styles.titlebox}>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate("manageDefault");
              }}
            >
              <View style={styles.backicon}>
                <AntDesign name="close" size={34 * rem} color="white" />
              </View>
            </TouchableOpacity>

            <Text style={styles.upperlinewordbig}>編輯餐廳</Text>
          </View>
        </SafeAreaView>
        <Text style={styles.inword}>餐廳名稱</Text>
        <TextInput style={styles.textinput} />
        <Text style={styles.inword}>種類</Text>
        <Picker
          //selectedValue={this.state.hand}
          //onValueChange={(hand) => this.setState({ hand })}
          style={styles.selectbox}
          mode="dropdown"
        >
          <Picker.Item label="中式" value="east" />
          <Picker.Item label="西式" value="west" />
          <Picker.Item label="日韓" value="jaKo" />
          <Picker.Item label="飲料" value="drink" />
          <Picker.Item label="甜點" value="dessert" />
          <Picker.Item label="宵夜" value="night" />
        </Picker>
        <Picker
          //selectedValue={this.state.hand}
          //onValueChange={(hand) => this.setState({ hand })}
          style={styles.selectbox}
          mode="dropdown"
        >
          <Picker.Item label="早餐" value="breakfast" />
          <Picker.Item label="簡餐" value="simple" />
          <Picker.Item label="便當" value="bang" />
          <Picker.Item label="麵食" value="noodles" />
          <Picker.Item label="滷味" value="sch" />
          <Picker.Item label="炸物" value="fried" />
          <Picker.Item label="火鍋" value="hotpot" />
        </Picker>
        <Picker
          //selectedValue={this.state.hand}
          //onValueChange={(hand) => this.setState({ hand })}
          style={styles.selectbox}
          mode="dropdown"
        >
          <Picker.Item label="大吃" value="big" />
          <Picker.Item label="中吃" value="med" />
          <Picker.Item label="小吃" value="sma" />
          <Picker.Item label="校內" value="sch" />
        </Picker>
        <View style={styles.confirmbutton}>
          <Button
            title="確定"
            color="#F5BC5C"
            onPress={() => {
              this.props.navigation.navigate("manageDefault");
            }}
          />
        </View>
      </View>
    );
  }
}

export default editDefault;

const windowWidth = Dimensions.get("window").width;
const rem = windowWidth / 380;
const styles = StyleSheet.create({
  //for screen title area
  titlebox: {
    flexDirection: "row",
    backgroundColor: "#F5BC5C",
    height: 46 * rem,
    width: "100%",
    padding: 5 * rem,
  },
  backicon: {
    marginLeft: "10%",
  },
  upperlinewordbig: {
    fontSize: 22 * rem,
    color: "white",
  },
  confirmbutton: {
    margin: "6%",
    width: "88%",
    aspectRatio: 100 / 12,
    marginTop: "4%",
    position: "absolute",
    bottom: 0,
  },
  inword: {
    marginLeft: "6%",
    marginTop: "4%",
    fontSize: 22 * rem,
    color: "#00000080",
  },
  textinput: {
    width: "88%", //寬度
    aspectRatio: 100 / 12, //高度
    backgroundColor: "white",
    borderColor: "#0000001A",
    borderWidth: 0.5,
    borderRadius: 5,
    fontSize: 20,
    marginLeft: "6%",
    marginTop: "4%",
  },
  selectbox: {
    width: "88%",
    color: "#00000040",
    backgroundColor: "white",
    borderColor: "#0000001A",
    borderWidth: 0.5,
    marginLeft: "6%",
    marginTop: "4%",
  },
});
