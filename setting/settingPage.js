import React from "react";
import {
  View,
  StyleSheet,
  Text,
  SafeAreaView,
  StatusBar,
  TouchableOpacity,
  Dimensions,
} from "react-native";
import { MaterialIcons } from "@expo/vector-icons";

class settingPage extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, backgroundColor: "#FFFCF9" }}>
        <SafeAreaView //flexgrow:調整每個物件的占比=>1:8:1
          style={{
            paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0, //對齊上面
            flexDirection: "row", //排列方向:橫的
          }}
        >
          <View style={styles.titlebox}>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate("mainPage");
              }}
            >
              <View style={styles.backicon}>
                <MaterialIcons
                  name="arrow-back"
                  size={34 * rem}
                  color="white"
                />
              </View>
            </TouchableOpacity>

            <Text style={styles.upperlinewordbig}>設定</Text>
          </View>
        </SafeAreaView>
        <TouchableOpacity
          onPress={() => {
            this.props.navigation.navigate("manageDefault");
          }}
        >
          <Text style={styles.text}>管理預設餐廳</Text>
        </TouchableOpacity>
        <Text style={styles.Line}></Text>
        <TouchableOpacity
          onPress={() => {
            this.props.navigation.navigate("reportPage");
          }}
        >
          <Text style={styles.text}>問題回報</Text>
        </TouchableOpacity>
        <Text style={styles.Line}></Text>
      </View>
    );
  }
}

export default settingPage;

const windowWidth = Dimensions.get("window").width;
const rem = windowWidth / 380;

const styles = StyleSheet.create({
  //for screen title area
  titlebox: {
    flexDirection: "row",
    backgroundColor: "#F5BC5C",
    height: 46 * rem,
    width: "100%",
    padding: 5 * rem,
  },
  backicon: {
    marginLeft: "10%",
  },
  upperlinewordbig: {
    fontSize: 22 * rem,
    color: "white",
  },
  text: {
    color: "#808080",
    marginLeft: "6%",
    marginTop: "4%",
    fontSize: 18 * rem,
  },
  Line: {
    height: 2,
    backgroundColor: "#0000001A",
    marginTop: "4%",
  },
});
