import * as React from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Platform,
  StatusBar,
  SafeAreaView,
  TextInput,
  Button,
  Alert,
  Dimensions,
  Image,
} from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import { MaterialIcons } from "@expo/vector-icons";
import { AntDesign } from "@expo/vector-icons";
import { Entypo } from "@expo/vector-icons";

class manageDefault extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, backgroundColor: "#FFFCF9" }}>
        <SafeAreaView
          style={{
            paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0, //對齊上面
            flexDirection: "row", //排列方向:橫的
          }}
        >
          <View style={styles.titlebox}>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate("settingPage")}
            >
              <View style={styles.backicon}>
                <MaterialIcons name="close" size={34 * rem} color="white" />
              </View>
            </TouchableOpacity>

            <Text style={styles.upperlinewordbig}>管理預設餐廳</Text>
          </View>
        </SafeAreaView>
        <View style={styles.searchbox}>
          <View style={styles.searchIcon}>
            <AntDesign name="search1" size={28 * rem} color="#00000080" />
          </View>

          <TextInput style={styles.searchInput} placeholder="搜尋餐廳" />
        </View>
        <Text style={styles.Line} />
        <ScrollView>
          <Text style={styles.selectT}>校內</Text>
          <View style={styles.item}>
            <Text style={styles.checkBoxTxt}>餐廳名稱</Text>
            <TouchableOpacity
              onPress={() =>
                Alert.alert("", "", [
                  {
                    text: "編輯",
                    onPress: () =>
                      this.props.navigation.navigate("editDefault"),
                  },
                  {
                    text: "刪除",
                    onPress: () =>
                      Alert.alert("要刪除嗎?", "", [
                        { text: "取消" },
                        { text: "刪除" },
                      ]),
                  },
                ])
              }
            >
              <View style={styles.edit}>
                <Entypo
                  name="dots-three-vertical"
                  size={22 * rem}
                  color="#00000080"
                />
              </View>
            </TouchableOpacity>
          </View>
          <View style={styles.item}>
            <Text style={styles.checkBoxTxt}>餐廳名稱</Text>
            <TouchableOpacity
              onPress={() =>
                Alert.alert("", "", [
                  {
                    text: "編輯",
                    onPress: () => navigation.navigate("editDefault"),
                  },
                  {
                    text: "刪除",
                    onPress: () =>
                      Alert.alert("要刪除嗎?", "", [
                        { text: "取消" },
                        { text: "刪除" },
                      ]),
                  },
                ])
              }
            >
              <Image
                style={styles.edit}
                source={require("../assets/edit.png")}
              />
            </TouchableOpacity>
          </View>
          <View style={styles.item}>
            <Text style={styles.checkBoxTxt}>餐廳名稱</Text>
            <TouchableOpacity
              onPress={() =>
                Alert.alert("", "", [
                  {
                    text: "編輯",
                    onPress: () => navigation.navigate("editDefault"),
                  },
                  {
                    text: "刪除",
                    onPress: () =>
                      Alert.alert("要刪除嗎?", "", [
                        { text: "取消" },
                        { text: "刪除" },
                      ]),
                  },
                ])
              }
            >
              <Image
                style={styles.edit}
                source={require("../assets/edit.png")}
              />
            </TouchableOpacity>
          </View>
          <Text style={styles.selectT}>大吃</Text>
          <View style={styles.item}>
            <Text style={styles.checkBoxTxt}>餐廳名稱</Text>
            <TouchableOpacity
              onPress={() =>
                Alert.alert("", "", [
                  {
                    text: "編輯",
                    onPress: () => navigation.navigate("editDefault"),
                  },
                  {
                    text: "刪除",
                    onPress: () =>
                      Alert.alert("要刪除嗎?", "", [
                        { text: "取消" },
                        { text: "刪除" },
                      ]),
                  },
                ])
              }
            >
              <Image
                style={styles.edit}
                source={require("../assets/edit.png")}
              />
            </TouchableOpacity>
          </View>
          <View style={styles.item}>
            <Text style={styles.checkBoxTxt}>餐廳名稱</Text>
            <TouchableOpacity
              onPress={() =>
                Alert.alert("", "", [
                  {
                    text: "編輯",
                    onPress: () => navigation.navigate("editDefault"),
                  },
                  {
                    text: "刪除",
                    onPress: () =>
                      Alert.alert("要刪除嗎?", "", [
                        { text: "取消" },
                        { text: "刪除" },
                      ]),
                  },
                ])
              }
            >
              <Image
                style={styles.edit}
                source={require("../assets/edit.png")}
              />
            </TouchableOpacity>
          </View>
          <View style={styles.item}>
            <Text style={styles.checkBoxTxt}>餐廳名稱</Text>
            <TouchableOpacity
              onPress={() =>
                Alert.alert("", "", [
                  {
                    text: "編輯",
                    onPress: () => navigation.navigate("editDefault"),
                  },
                  {
                    text: "刪除",
                    onPress: () =>
                      Alert.alert("要刪除嗎?", "", [
                        { text: "取消" },
                        { text: "刪除" },
                      ]),
                  },
                ])
              }
            >
              <Image
                style={styles.edit}
                source={require("../assets/edit.png")}
              />
            </TouchableOpacity>
          </View>
          <Text style={styles.selectT}>中吃</Text>
          <View style={styles.item}>
            <Text style={styles.checkBoxTxt}>餐廳名稱</Text>
            <TouchableOpacity
              onPress={() =>
                Alert.alert("", "", [
                  {
                    text: "編輯",
                    onPress: () => navigation.navigate("editDefault"),
                  },
                  {
                    text: "刪除",
                    onPress: () =>
                      Alert.alert("要刪除嗎?", "", [
                        { text: "取消" },
                        { text: "刪除" },
                      ]),
                  },
                ])
              }
            >
              <Image
                style={styles.edit}
                source={require("../assets/edit.png")}
              />
            </TouchableOpacity>
          </View>
          <View style={styles.item}>
            <Text style={styles.checkBoxTxt}>餐廳名稱</Text>
            <TouchableOpacity
              onPress={() =>
                Alert.alert("", "", [
                  {
                    text: "編輯",
                    onPress: () => navigation.navigate("editDefault"),
                  },
                  {
                    text: "刪除",
                    onPress: () =>
                      Alert.alert("要刪除嗎?", "", [
                        { text: "取消" },
                        { text: "刪除" },
                      ]),
                  },
                ])
              }
            >
              <Image
                style={styles.edit}
                source={require("../assets/edit.png")}
              />
            </TouchableOpacity>
          </View>
          <View style={styles.item}>
            <Text style={styles.checkBoxTxt}>餐廳名稱</Text>
            <TouchableOpacity
              onPress={() =>
                Alert.alert("", "", [
                  {
                    text: "編輯",
                    onPress: () => navigation.navigate("editDefault"),
                  },
                  {
                    text: "刪除",
                    onPress: () =>
                      Alert.alert("要刪除嗎?", "", [
                        { text: "取消" },
                        { text: "刪除" },
                      ]),
                  },
                ])
              }
            >
              <Image
                style={styles.edit}
                source={require("../assets/edit.png")}
              />
            </TouchableOpacity>
          </View>
          <Text style={styles.selectT}>小吃</Text>
          <View style={styles.item}>
            <Text style={styles.checkBoxTxt}>餐廳名稱</Text>
            <TouchableOpacity
              onPress={() =>
                Alert.alert("", "", [
                  {
                    text: "編輯",
                    onPress: () => navigation.navigate("editDefault"),
                  },
                  {
                    text: "刪除",
                    onPress: () =>
                      Alert.alert("要刪除嗎?", "", [
                        { text: "取消" },
                        { text: "刪除" },
                      ]),
                  },
                ])
              }
            >
              <Image
                style={styles.edit}
                source={require("../assets/edit.png")}
              />
            </TouchableOpacity>
          </View>
          <View style={styles.item}>
            <Text style={styles.checkBoxTxt}>餐廳名稱</Text>
            <TouchableOpacity
              onPress={() =>
                Alert.alert("", "", [
                  {
                    text: "編輯",
                    onPress: () => navigation.navigate("editDefault"),
                  },
                  {
                    text: "刪除",
                    onPress: () =>
                      Alert.alert("要刪除嗎?", "", [
                        { text: "取消" },
                        { text: "刪除" },
                      ]),
                  },
                ])
              }
            >
              <Image
                style={styles.edit}
                source={require("../assets/edit.png")}
              />
            </TouchableOpacity>
          </View>
          <View style={styles.item}>
            <Text style={styles.checkBoxTxt}>餐廳名稱</Text>
            <TouchableOpacity
              onPress={() =>
                Alert.alert("", "", [
                  {
                    text: "編輯",
                    onPress: () => navigation.navigate("editDefault"),
                  },
                  {
                    text: "刪除",
                    onPress: () =>
                      Alert.alert("要刪除嗎?", "", [
                        { text: "取消" },
                        { text: "刪除" },
                      ]),
                  },
                ])
              }
            >
              <Image
                style={styles.edit}
                source={require("../assets/edit.png")}
              />
            </TouchableOpacity>
          </View>
          <Text style={styles.selectT}>校內</Text>
          <View style={styles.item}>
            <Text style={styles.checkBoxTxt}>餐廳名稱</Text>
            <TouchableOpacity
              onPress={() =>
                Alert.alert("", "", [
                  {
                    text: "編輯",
                    onPress: () => navigation.navigate("editDefault"),
                  },
                  {
                    text: "刪除",
                    onPress: () =>
                      Alert.alert("要刪除嗎?", "", [
                        { text: "取消" },
                        { text: "刪除" },
                      ]),
                  },
                ])
              }
            >
              <Image
                style={styles.edit}
                source={require("../assets/edit.png")}
              />
            </TouchableOpacity>
          </View>
          <View style={styles.item}>
            <Text style={styles.checkBoxTxt}>餐廳名稱</Text>
            <TouchableOpacity
              onPress={() =>
                Alert.alert("", "", [
                  {
                    text: "編輯",
                    onPress: () => navigation.navigate("editDefault"),
                  },
                  {
                    text: "刪除",
                    onPress: () =>
                      Alert.alert("要刪除嗎?", "", [
                        { text: "取消" },
                        { text: "刪除" },
                      ]),
                  },
                ])
              }
            >
              <Image
                style={styles.edit}
                source={require("../assets/edit.png")}
              />
            </TouchableOpacity>
          </View>
          <View style={styles.item}>
            <Text style={styles.checkBoxTxt}>餐廳名稱</Text>
            <TouchableOpacity
              onPress={() =>
                Alert.alert("", "", [
                  {
                    text: "編輯",
                    onPress: () => navigation.navigate("editDefault"),
                  },
                  {
                    text: "刪除",
                    onPress: () =>
                      Alert.alert("要刪除嗎?", "", [
                        { text: "取消" },
                        { text: "刪除" },
                      ]),
                  },
                ])
              }
            >
              <Image
                style={styles.edit}
                source={require("../assets/edit.png")}
              />
            </TouchableOpacity>
          </View>
        </ScrollView>
        <View
          style={{
            width: "100%",
            alignItems: "flex-end",
            bottom: 0,
            position: "absolute",
          }}
        >
          <TouchableOpacity
            style={styles.plusview}
            onPress={() => this.props.navigation.navigate("addDefault")}
          >
            <View style={styles.plusicon}>
              <AntDesign name="plus" size={30 * rem} color="white" />
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default manageDefault;
const windowWidth = Dimensions.get("window").width;
const rem = windowWidth / 380;

const styles = StyleSheet.create({
  //for screen title area
  titlebox: {
    flexDirection: "row",
    backgroundColor: "#F5BC5C",
    height: 46 * rem,
    width: "100%",
    padding: 5 * rem,
  },
  backicon: {
    marginLeft: "10%",
  },
  upperlinewordbig: {
    fontSize: 22 * rem,
    color: "white",
  },
  //for chooseToEat
  searchbox: {
    flexDirection: "row",
    width: "100%",
    marginTop: "4%",
    textAlignVertical: "center",
  },
  searchInput: {
    fontSize: 18 * rem,
    marginLeft: "6%",
  },
  searchIcon: {
    marginLeft: "6%",
  },
  Line: {
    marginTop: "4%",
    height: 2,
    backgroundColor: "#00000040",
  },
  //for options
  selectT: {
    marginTop: "4%",
    marginLeft: "6%",
    fontSize: 20 * rem,
    color: "#00000080",
    marginBottom: "2%",
  },
  item: {
    flexDirection: "row",
    marginTop: "2%",
    marginLeft: "6%",
  },
  edit: {
    height: 28,
    width: 28,
    marginTop: "1.5%",
    marginLeft: "76%",
  },
  checkBoxTxt: {
    color: "#00000080",
    fontSize: 19 * rem,
  },
  plusview: {
    backgroundColor: "#F5BC5C",
    width: 60 * rem,
    height: 60 * rem,
    borderRadius: 30 * rem,
    justifyContent: "center",
    alignItems: "center",
    marginBottom: "6%",
    marginRight: "6%",
  },
  plusicon: {},
});
